export const book_loaded = (newbooks) =>{
    return{
        type :'Book_loaded', 
        payload : newbooks
    };
};

export const book_Requested = () =>{
      return{
          type: "Book_requested"
      };
};
export const book_addedToCart = (book) =>{
      return{
          type: "Book_addedToCart",
          payload : book  
      };  
};
export const book_removedFromCart = (book) =>{
     return{
         type : "Book_removedFromCart",
         payload : book  
     };   
}; 
export const allBook_removedFromCart = (book) =>{
     return{
         type : "AllBook_removedFromCart",
         payload : book 
     };
};  


