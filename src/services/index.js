export default class BookStoreServices {
  data = [
    {
      id: 1,
      title: 'Մտքիս կծիկը',
      author: 'Գրիգոր Մաչանենց',
      price: 4500,
      coverImage: '../../images/0asfw5hgbyx82ecd.jpg'
    },
    {
      id: 2,
      title: 'Կարմիրը և սևը',
      author: 'Ստենդալ',
      price: 5990,
      coverImage: '../../images/1huz0yqj2xwp895a.jpg'
    },
    {
      id: 3,
      title: 'Ցնորքից մինչև իրականություն',
      author: 'Վարդինե Իսահակյան',
      price: 3300,
      coverImage: '../../images/13pir2fc7n0u9ovh.jpg'
    },
    {
      id: 4,
      title: 'Հատուցում',
      author: 'Էմա Գլաս',
      price: 2400,
      coverImage: '../../images/cdomrkewz8tai7p2.jpg'
    },
    {
      id: 5,
      title: 'Բաղրամյան քսանվեց',
      author: 'Սամվել Խաչատրյան',
      price: 2000,
      coverImage: '../../images/di7nryfwzpmj8gha.jpg'
    },
    {
      id: 6,
      title: 'Վարդինե Իսահակյան',
      author: 'Վարդինե Իսահակյան',
      price: 3000,
      coverImage: '../../images/jekrc734dpliz5yw.jpg'
    },
    {
      id: 7,
      title: 'Որբանոց',
      author: ' Էդգար Կոստանդյան',
      price: 3000,
      coverImage: '../../images/l8jk7du1zsvtrixe.jpg'
    },
    {
      id: 8,
      title: 'Մենաստան',
      author: ' Զախար Պրիլեպին',
      price: 5990,
      coverImage: '../../images/1huz0yqj2xwp895a.jpg'
    },
    {
      id: 9,
      title: 'Հուդա',
      author: 'Ամոս Օզ',
      price: 4990,
      coverImage: '../../images/wgxv0hmosyl17pe4.jpg'
    },
  ];
  getBooks() {
    return new Promise((resolve) => {
      setTimeout(() => {
         resolve(this.data);
      }, 700);
    });
  }
}