import React from "react"
import {
    BrowserRouter,
    Switch,
    Route,
  } from "react-router-dom";

import Header from "../header"; 
import Home from "../home"
import BookList from "../book-list";

import "./index.css"

const App = () =>{
    return(
    <div className="app">
         <BrowserRouter>
            <Header />
               <Switch>
                   <Route path="/"  exact component={Home} />
                   <Route path="/books" component={BookList}/>
               </Switch>
         </BrowserRouter>
    </div>
    )
};


export default App