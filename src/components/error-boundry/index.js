import React from "react";
import ErrorIndicator from "../error-indicator";

class ErrorBoundry extends React.Component{
     constructor(){
         super()
         this.state = {
            hasError : false,
          };
     }
    componentDidCatch(){
        this.setState({
            hasError : true, 
        });
    }

    render(){
        if(this.state.hasError){
           return <ErrorIndicator />
        }

     return(
        <div>
           {this.props.children}
        </div>
        )
    }
}
export default ErrorBoundry 