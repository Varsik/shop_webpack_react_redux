import React from "react"
import { connect } from "react-redux"
import { book_addedToCart,  
         book_removedFromCart, 
         allBook_removedFromCart } from "../../actions"
         
import "./index.css"

const Cart = ({cartItem, onDelete, onIncrease,onDecrease }) => {
        return (
            <div className="cart">
                <table>
                    <tbody>
                            <tr>
                                <th>Item</th>
                                <th>Count</th>
                                <th>Price</th>
                                <th>Image</th>
                            </tr>                                   
                                {cartItem.map((item,index)=>{
                                   return( 
                                    <tr key={index}>
                                    <td> 
                                           {item.author}  
                                        <i className="fas fa-angle-left"></i>
                                        <i className="fas fa-angle-left"></i>
                                             {item.title}
                                        <i className="fas fa-angle-right"></i>
                                        <i className="fas fa-angle-right"></i>
                                     </td>
                                    <td> {item.count} </td>
                                    <td>{item.total} {item.price} $</td>
                                    <td><img  src={item.coverImage} /></td>
                                    <td> 
                                    <button
                                        onClick={() => onDelete(item)}                                     
                                        className="btn">
                                        <i className="fas fa-trash-alt"></i>
                                    </button>
                                    <button
                                        onClick={() => onIncrease(item)}
                                        className="btn">
                                        <i className="fa fa-plus-circle" />
                                    </button>
                                    <button
                                        onClick={() => onDecrease(item)}
                                        className="btn">
                                        <i className="fa fa-minus-circle" />
                                    </button></td>
                                    </tr>  
                                    )
                                })}
                      </tbody>
                    </table>
            </div>
        )
    }
const mapstateToProps = (state) => {
      return{
        cartItem: state.cartItem
      };
};
const  mapDispatch =  {  
    onIncrease : book_addedToCart,  
    onDecrease :book_removedFromCart, 
    onDelete : allBook_removedFromCart
};

export default connect(mapstateToProps, mapDispatch)(Cart)