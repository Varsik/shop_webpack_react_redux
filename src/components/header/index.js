import  React from "react"
import {NavLink} from "react-router-dom"

import "./index.css"

class Header extends React.Component{
    render(){
        return (
            <div>
                <nav>
                    <NavLink to="/" className="logo">
                       Home
                    </NavLink>
   
                    <NavLink to="/books" className="cat-item">
                        5 item ($200)
                    </NavLink>
                </nav>
            </div>
        )
    }
}
export default Header