import  React from "react"
import { connect } from "react-redux"

import { book_loaded, 
         book_addedToCart, 
         book_Requested , 
}from "../../actions"
import BookStoreServices from "../../services"
import Spinner from "../spinner"
import Cart from "../shopping-cart-table"

import "./index.css"

class BookList extends React.Component{

    componentDidMount(){
        const  bookStoreServices = new  BookStoreServices()
        this.props.book_Requested() 
        bookStoreServices.getBooks()
        .then((books)=>{
            this.props.book_loaded(books)
        })
    }
    render(){
        return(
            <div >  
             {this.props.loading ? 
               <Spinner  /> :
                  <div className="booksItems">
                  {this.props.books.map((item,index)=>{
                      return(
                          <div key={index} className="book">
                             <img src={item.coverImage}/>
                               <p>{item.author}</p>
                               <p>{item.title}</p>
                               <p>{item.price} $</p>
                               <button 
                                   onClick={()=>this.props.book_addedToCart(item)}>
                                Add To Cart
                               </button>
                          </div>
                      )})} 
                    <Cart />
                    </div>
                }    
            </div>
        )
    }
}
const mapStateToProps = (state) =>{
      return{
          books :  state.books,
          loading : state.loading
      };
};
const  mapDispatch =  {  
     book_loaded,  
     book_addedToCart,
     book_Requested,
    };
export default  connect(mapStateToProps, mapDispatch)(BookList)