const initialState = {
  books : [],
  cartItem :[],
  loading : true,
};

const updateCartItems = (cartItems, item, idx) => {

 if(item.count == 0) {
  return[
    ...cartItems.slice(0, idx),
   ...cartItems.slice(idx + 1),
 ];
 }

 if(idx === -1){
  return[
    ...cartItems, 
    item
  ];
 }
  
   return[
    ...cartItems.slice(0, idx),
    item,
   ...cartItems.slice(idx + 1),
 ];

};

const updateCartItem = (bookItem, item = {}, quantity)=>{
    const {
      id = bookItem.id,
      count = 0,
      title = bookItem.title,
      author =  bookItem.author,
      coverImage = bookItem.coverImage,
      total = 0 } = item;
  
    return {
      id,
      title,
      coverImage,
      author,
      count: count + quantity,
      total: total + quantity * bookItem.price
    };
  }

const updateOrder = (state, books, quantity) =>{
         const bookItem =  books;
         const book = state.books.find((item)=> item.id === bookItem.id);
         const itemIndex = state.cartItem.findIndex((item)=> item.id === bookItem.id);
         const item =  state.cartItem[itemIndex];
        const newItem = updateCartItem(book, item, quantity)
        return{
          ...state,
           cartItem : updateCartItems(state.cartItem,  newItem, itemIndex)
        };
  
      
}
const reducer = (state = initialState, action)=>{
 switch(action.type){
     case 'Book_loaded' :
       return{
         ...state,
         books : action.payload,
         loading : false
       };
       case "Book_requested" : 
         return{
           ...state,
           books : [ ],
           loading : true,
         };      
       
       case "Book_addedToCart" :
           return updateOrder(state, action.payload, 1)
         case "Book_removedFromCart" :
          return updateOrder(state, action.payload, -1)
        case "AllBook_removedFromCart" :
          const  cartItem = state.cartItem.find((item)=> item.id === action.payload.id);
          return updateOrder(state, action.payload, -cartItem.count)
     default :
         return state;
 }    
};

export default reducer 